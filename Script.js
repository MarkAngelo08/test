/**
 *  MIT License
    Copyright (c) 2022 Mark Angelo Paduhilao
    https://markangelopaduhilao.epizy.com/
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

/**(NOTES)
 * 1. Read Notes!
 * 2. The color is split into 4 (hue, whiteness, blackness and opacity)
 * 3. The purpose of spliting the color into cmyka is that you can't 
 *      get all of this in just a color, it'll ruined the output since as the color adjust so does the 3 class
 * 4. The color from gdata is the output of 4 class (hue, sturation, blackness and opacity) this 
 *      is passed to the setOutputColor(function)
 * 6. Make sure to pass final value of cmyka to setOutputColor
 * 7. You can now use main as variable name same with cmyk_color_pckr
 * 8. Main and cmyk_color_pckr variable name is the same
 * 9. Make sure to set targetElement as DOM not Jquery
 * 10. You can't trigger input event from setOutputColor since it'll trigger the initTopNav input event
 *     and could cause performance or infinite recursive error. This only trigger change event.
 * 11. If you want to listen for color output you can use change event.
 * 12. Don't use targetElement to private and public function you can only use this to initObserver as long as you can.
 * 13. Don't add ready to the listener, since the callback need to call even color picker already ready or about to be ready.
 * 14. Iframe size depends on panel size
 * 15. Whenever the mouse is moving or dragging, do not call e.preventDefault; this will affect the position stopped once the cursor is outside of the iframe.
 * 16. When the picker is visible, it is not possible to update the input background color; however, the color is still applied even if the background is not updated.
 *      see at initObserver. 
 * 17. Once the transparent is disabled the output color will be set RGB format only, 
 *      search gdata.transparentEnabled to check
 * 18. Do not use offset from jquery, instead use css only
 * 19. w3color and color are stored in setColor and setOutputColor for realtime update and for performance purpose
 * 20. Extensions are linked to autoHidePanel and initExtension
 * 21. You can't off ready
 */
(()=>{
    //to stop from creating if already added
    if(window.markCollection && window.markCollection.cmykColorPicker){
        return;
    }

    const global_data = {
        /**default color */
        //this was store in setOutputColor
        color: "blue",
        w3color: window.w3color("blue"),
        //---
        //for visibility
        //since we're using animation it is possible to not immediatly detect the iframe visibility
        isVisible : false,
        /**
         * This to ensure once the user disable transparent
         * the input color will also set to non-transparent
         */
        transparentEnabled: true,
        /**this will be the default color */
        defaultColor:"blue",
        hue: 0,
        //default cmyk
        cyan:0,
        magenta:1,
        yellow:1,
        key:0,
        opacity : 1,
        /**default type */
        colorOption: "cmyk",
        //you can only initialize this once
        onready:null,
        /**target element on which triggered the panel to open*/
        targetElement : null,
        //for main iframe properties
        iframeProperties:{
            iframe: null,
            //hold the controls
            panel: null,
            //holds the iframe document
            document: null
        },
        //this store all the extension available
        extensions:{},
    };

    /**override w3color here */
    function w3color(color){
        return window.w3color(color);
    }

    //this fix the event position for android and desktop 
    function fixEventPosition(e){
        var mx = parseFloat(e.pageX),
            mx = isNaN(mx) ? e.originalEvent.touches[0].pageX : mx,
            my = parseFloat(e.pageY),
            my = isNaN(my) ? e.originalEvent.touches[0].pageY : my;
        e.pageX=mx;
        e.pageY=my;
    }

    function copyToClipboard(str){

        //check if defined
        if(navigator && navigator.clipboard && navigator.clipboard.writeText){
            navigator.clipboard.writeText(str);
        }
        //note this may cause picker to close since we focus 
        //main window or the input attached to the main window
        else if(document.execCommand){
            var inp = document.createElement("input");
            $("body").append(inp);
            //this to avoid from interfering the outside style
            $(inp).css({"position":"fixed","opacity":0});
            $(inp).val(str);
            $(inp).focus();
            
            //copy and remove after
            $(inp).ready(()=>{
                $(inp).select();
                document.execCommand("copy");
                $(inp).remove();
            });
        }
    }

    /**
     * This calculate visible side between rect2 and the viewport and return
     * which side can be put rect
     * @param {*} target_elem - find the element position based on target position
     * @param {*} elem - to attach from target element
     * @param {*} adjust set the adjust on where visible is focused for 
     *  instance if focused on x coordinate that will be adjusted
     * @param viewport default([0,0,window.innerWidth,window.innerHeight])
     * @return [x,y] 
    */
    function calcVisibleSide(target_elem, elem,adj=8){
        var trect = target_elem.getBoundingClientRect(),
            erect = elem.getBoundingClientRect();
        const sort = {
            "left":[
                [["left","right"], ["top","top"]],
                [["left","right"], ["center","center"]],
                [["left","right"], ["bottom","bottom"]]
            ],
            "top":[
                [["left","left"], ["top","bottom"]],
                [["center","center"], ["top","bottom"]],
                [["right","right"], ["top","bottom"]]
            ],
            "right":[
                [["right","left"], ["top","top"]],
                [["right","left"], ["center","center"]],
                [["right","left"], ["bottom","bottom"]]
            ],
            "bottom":[
                [["left","left"], ["bottom","top"]],
                [["center","center"], ["bottom","top"]],
                [["right","right"], ["bottom","top"]]
            ],
            //if all of them not found then we can use this corner
            "corner":[
                //for right to left corner
                [["right","left"], ["top","bottom"]],
                [["right","left"], ["bottom","top"]],
                //for left to right corner
                [["left","right"], ["top","bottom"]],
                [["left","right"], ["bottom","top"]],
                //for bottom to top corner
                [["right","left"], ["bottom","top"]],
                [["left","right"], ["bottom","top"]],
                //for top to bottom
                [["right","left"], ["top","bottom"]],
                [["left","right"], ["top","bottom"]],
            ]
        };

        //support center only by center
        function getPosition(r, r2, side){
            var x_side = side[0],
                y_side = side[1];
            var pos = [];

            //left set to right
            if(x_side[0] == "left" && x_side[1] == "right"){
                pos[0] = r.right;
            }//right set to left
            else if(x_side[0] == "right" && x_side[1] == "left"){
                pos[0] = r.left - r2.width;
            }//right set to right
            else if(x_side[0] == "right" && x_side[1] == "right"){
                pos[0] = r.left + r.width - r2.width;
            }//center set to center
            else if(x_side[0] == "center" && x_side[1] == "center"){
                pos[0] = r.left + r.width / 2 - r2.width / 2;
            }else{//left set to left
                pos[0] = r.left;
            }


            //top set to bottom
            if(y_side[0] == "top" && y_side[1] == "bottom"){
                pos[1] = r.bottom;
            }//bottom to top
            else if(y_side[0] == "bottom" && y_side[1] == "top"){
                pos[1] = r.top - r2.height;
            }//bottom set to bottom
            else if(y_side[0] == "bottom" && y_side[1] == "bottom"){
                pos[1] = r.bottom - r2.height;
            }//center set to center
            else if(y_side[0] == "center" && y_side[1] == "center"){
                pos[1] = r.top + r.height / 2 - r2.height / 2;
            }else{//top set to top
                pos[1] = r.top;
            }
            
            return pos;
        }
        
        //Demo on how get position work using sort side
        // var count = 0;
        // for(var key in sort){
        //     for(var i in sort[key]){
        //         (()=>{
        //             var side = sort[key][i];
        //             var pos = getPosition(trect, erect, side);
        //             console.log(pos);
                    
        //             setTimeout(()=>{
        //                 $(elem).offset({
        //                     left:pos[0],
        //                     top:pos[1]
        //                 });
        //             },count * 1000);
        //         })();
        //         count += 1;
        //     }
        // }

        //begin to find visible side
        for(var key in sort){
            for(var i in sort[key]){
                var side = sort[key][i];
                var pos = getPosition(trect, erect, side);
                var rect = [pos[0], pos[1], pos[0] + erect.width, pos[1] + erect.height];
                
                //check if not collide from the outside
                if(rect[0] >= 0 && rect[1] >= 0 && 
                    rect[2] <= window.innerWidth && rect[3] <= window.innerHeight){
                    
                    //for adjustment
                    if(key == "left"){
                        pos[0] += adj;
                    }else if(key == "right"){
                        pos[0] -= adj;
                    }else if(key == "top"){
                        pos[1] += adj;
                    }else if(key == "bottom"){
                        pos[1] -= adj;
                    }
                    return pos;
                }
            }
        }

        //no found visible
        return null;
    }
    
    /*to front only work on element who's child by body and grandchils i*/
    function toFront(elem){
        var max_idx = 0;
        //find all children
        var children = $("*");
        children.each((idx)=>{
            var child = children[idx];
            var idx = isNaN(parseInt($(child).css("z-index"))) ? 0 : parseInt($(child).css("z-index"));
            if(idx > max_idx){
                max_idx = idx + 1;
            }
        });
        $(elem).css("z-index",String(max_idx + 1));
    }

    function isBrowserMobile(){
        var isMobile=false;
        if(('ontouchstart' in document.documentElement) &&
        //original webOS|Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini
        (/Android|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))){
          isMobile=true;
        } else {
         isMobile=false;
        }
      return isMobile
    }
    
    //https://reactgo.com/get-screen-size-javascript/
    function getScreenSize(include_task_bar = false){
        if(include_task_bar){
            //get the total width and height of the user’s screen including taskbar
            return {
                width: window.screen.width,
                height: window.screen.height
            }
        }
        return {
            width: window.screen.availWidth,
            height: window.screen.availHeight
        }
    }

    function isW3color(obj){
        return typeof(obj.toHsl) == "function" && typeof(obj.toRgbaString) == "function";
    }

    /**PUBLIC*/
    /**
     * 
     * @param {*} adjuster - element that contains data-type adjuster
     * @param {*} pos - position 0 to 1 only
     */
    function setAdjusterThumbPosition(adjuster, pos){
        var thumb = $(adjuster).find("> .thumb").first();
        //convert position as val
        var val = pos;
        
        if($(adjuster).is("[data-angle=vertical]")){
            //make sure to get adjuster's height rather than rectangle since this calculate the outer size
            var ypos = ($(adjuster).height() - $(thumb).outerHeight()) * val,
                ypos = ypos / $(adjuster).height();

            //set new value
            $(thumb).css("top",(ypos*100)+"%");
            $(thumb).attr("title",((pos).toFixed(3) *100)+"%");
            $(adjuster).attr("data-value",val);
        }else{//otherwise horizontal
            var xpos = ($(adjuster).width() - $(thumb).outerWidth()) * val,
            //use relative position
                xpos = xpos / $(adjuster).width();
            
            //set new value
            $(thumb).css("left",(xpos * 100)+"%");
            $(thumb).attr("title",((pos).toFixed(3) * 100)+"%");
            $(adjuster).attr("data-value",val);
        }
    }
    
    /**(Performance: faster)
     * This set transparent, saturation, and lightness background color or gradient color. 
     * Make sure to not contains tsl value, only the hue value. Ex: value hsl(n, 100%, 50%)
     */
    function setTAdjusterBackgroundColor(color){
        const gdata = global_data;
        var panel = gdata.iframeProperties.panel;
        var center_nav = $(panel).find("> .container > .center-nav").first();

        var trans_adjuster = $(center_nav).find(".transparent-adjuster").first();
        
        /**set the transparent */
        $(trans_adjuster).find(".color-gradient").css({
            "background-image":"-webkit-gradient(linear,left top, left bottom,from("+color+"),to(transparent))",
            "background-image":"-o-linear-gradient("+color+",transparent)",
            "background-image":"linear-gradient("+color+",transparent)"
        });
    }

    /**(Performance: slower) - because you're updating all the adjuster's value
     * This set new color, and update all the UI based on window.w3color
     * @param {*} color 
     */
    function setColor(color){
        const gdata = global_data;
        var panel = gdata.iframeProperties.panel;
        var center_nav = $(panel).find("> .container > .center-nav").first(),
            bottom_nav = $(panel).find("> .container > .bottom-nav").first();
        var w3_color = window.w3color(color);
        //if transparent is disabled set only the color format to RGB
            w3_color.opacity = gdata.transparentEnabled ? w3_color.opacity : 1;
        var cyan_adjuster = $(bottom_nav).find(".cyan-adjuster").first(), 
            magenta_adjuster = $(bottom_nav).find(".magenta-adjuster").first(),
            yellow_adjuster = $(bottom_nav).find(".yellow-adjuster").first(),
            key_adjuster = $(bottom_nav).find(".key-adjuster").first(),
            trans_adjuster = $(center_nav).find(".transparent-adjuster").first();

        const hwb = w3_color.toHwb(),
            opacity = 1 - w3_color.opacity;
        const cmyk = w3_color.toCmyk();
        
        /**transparent */
        setAdjusterThumbPosition(trans_adjuster, opacity);
        /**cyan */
        setAdjusterThumbPosition(cyan_adjuster, cmyk.c);
        /**magenta */
        setAdjusterThumbPosition(magenta_adjuster, cmyk.m);
        /**yellow */
        setAdjusterThumbPosition(yellow_adjuster, cmyk.y);
        /**key(black) */
        setAdjusterThumbPosition(key_adjuster, cmyk.k);

        //update TSL background coclor
        setTAdjusterBackgroundColor(w3_color.toRgbString());
        
        //update the ouput color
        setOutputColor(w3_color);
        
        //set the data
        gdata.color = w3_color.toRgbaString();
        gdata.cyan = cmyk.c;
        gdata.magenta = cmyk.m;
        gdata.yellow = cmyk.y;
        gdata.key = cmyk.k;//black
        
        gdata.w3color = w3_color;
        gdata.opacity = w3_color.opacity;
    }
    
    /**
     * 
     * @param {*} color color
     * @param {*} option [hsl, hex, rgb, rgba, hwb, cmyk]
     * @return RGBA if there is an opacity included, otherwise it depends on the option
     */
    function colorTo(color, option){
        var w3_color = color.constructor === Object ? color : window.w3color(color);
        var option = w3_color.opacity < 1 ? "rgba" : option;
        
        switch(option){
            case "hsl":
                return w3_color.toHslString();
            case "hex":
                return w3_color.toHexString();
            case "rgb":
                return w3_color.toRgbString();
            case "rgba":
                return w3_color.toRgbaString();
            case "hwb":
                return w3_color.toHwbString();
            case "cmyk":
                return w3_color.toCmykString();
        }

        return color;
    }

    //This set top-nav and output element color
    function setOutputColor(color){
        const gdata = global_data;
        var panel = gdata.iframeProperties.panel,
            top_nav = $(panel).find("> .top-nav").first(),
            output = $(panel).find("> .container > .center-nav > .output").first();
        //make sure to check if w3color already
        var w3_color = isW3color(color) ? color : window.w3color(color);
        //check if transparent is disabled
            w3_color.opacity = gdata.transparentEnabled ? w3_color.opacity : 1;
        //HSL are used only to check if the background color is nearest to text color
        var hsl = w3_color.toHsl(),
            txt_color = hsl.l >= 0.6 ? "black" : "white",//default
            txt_color2 = hsl.l >= 0.6 || w3_color.opacity <= 0.5 ? "black" : "white";
        //get top nav text
        var top_nav_inp = $(top_nav).find("> .output-text").first();
        var color_opt = gdata.colorOption,
        //you can w3 color here
            parse_color = colorTo(w3_color, color_opt);
        //store previous color
        const prev_color = gdata.color;

        //store both color and its w3color
        gdata.w3color = w3_color;
        gdata.color = w3_color.toRgbaString();
        
        /**topnav contains rgba */
        $(top_nav).css("background-color",w3_color.toRgbaString());
        $(top_nav).attr("data-color",txt_color2);
        $(top_nav_inp).val(parse_color);
        $(top_nav_inp).attr("title",parse_color);
        
        //you can only trigger "change" if previous color and current color not matched
        if(gdata.color != prev_color){
            $(top_nav_inp).trigger("change");
        }
        
        /**output only contains rgb */
        $(output).css("background-color",w3_color.toRgbString());
        $(output).css("color",txt_color);
        if(w3_color.toName()){
            $(output).find("> .color-name").text(w3_color.toName());
        }else{
            $(output).find("> .color-name").text(w3_color.toHexString());
        }
    }
    
    //this returned focus element based on initAutoFocus
    //since you can get multiple focus, you can allow them to return into array by setting up the  allow_multiple
    //this is according to initAutoFocus
    function getFocusElement(allow_multiple=false){
        const gdata = global_data;
        var panel = gdata.iframeProperties.panel;
        if(allow_multiple){//set to array
            return $(panel).find("[data-autofocus][data-focus]").toArray();
        }
        return $(panel).find("[data-autofocus][data-focus]")[0];
    }
    
    /**
     * @param {*} target_elem target element to show
     * @returns null
     */
    function showPicker(target_elem){
        const gdata = global_data;
        var panel = gdata.iframeProperties.panel;
        var iframe = gdata.iframeProperties.iframe;
        //since we're using animation it is possible to not immediatly detect the iframe visibility
        const is_prev_visible = gdata.isVisible;

        //you must first open the iframe
        $(iframe).stop();//stop the animation
        $(iframe).fadeIn(300);
        //update iframe size
        $(iframe).css({
            width:$(panel).outerWidth(),
            height:$(panel).outerHeight()
        });
        //set iframe's content to focus
        iframe.contentWindow.focus();
        
        //make sure to add to front
        toFront(iframe);
        pos = !target_elem ? null : calcVisibleSide(target_elem, $(iframe)[0], 5);
        
        //if no target or screen width is less than 500 pixel we'll set this to center
        if(!pos || getScreenSize().width <= 500){
            //set to center
            $(iframe).css({
                "-webkit-transform":"translate(-50%,-50%)",
                "-ms-transform":"translate(-50%,-50%)",
                    "transform":"translate(-50%,-50%)",
                "left":"50%",
                "top":"50%"
            });
        }else{;
            $(iframe).css({
                "-webkit-transform":"none",
                "-ms-transform":"none",
                    "transform":"none"
            });
            //set panel position
            $(iframe).css({
                left: pos[0]+"px",
                top: pos[1]+"px"
            });
        }

        //add this before the trigger event for realtime update
        gdata.isVisible = true;
        //check if previous visibility value is not visible, then trigger the event
        if(!is_prev_visible){
            $(iframe).trigger("open");
        }
    }

    /**
     * Hide the panel
     */
    function hidePicker(smooth=true){
        const gdata = global_data;
        var iframe = gdata.iframeProperties.iframe;
        //since we're using animation it is possible to not immediatly detect the iframe visibility
        const is_prev_visible = gdata.isVisible;

        if(!smooth){
            $(iframe).stop();//stop the animation
            $(iframe).hide();
        }//otherwise
        else{
            //close the panel
            $(iframe).stop();//stop the animation
            $(iframe).fadeOut(300,()=>{
                //set z-index to 0 adter it close
                $(iframe).css("z-index",0);
            });
        }

        //add this before the trigger event for realtime update
        gdata.isVisible = false;
        //check if previous visibility value is visible, then trigger the event
        if(is_prev_visible){
            $(iframe).trigger("close");
        }
    }

    function isVisible(){
        const gdata = global_data;
        return $(gdata.iframeProperties.iframe).is(":visible");
    }

    /**
     * 
     * @param {*} elem target element to show
     * @param {*} smooth for animation
     * @returns null
     */
    function setPickerPositionTo(target_elem, smooth = false){
        const gdata = global_data;
        var iframe = gdata.iframeProperties.iframe;
        //can't open if no target
        if(!target_elem){return;}

        //you must first open the panel
        $(iframe).show();//stop the animation
        //make sure to add to front
        toFront(iframe);
        //before we calclualte the visible side
        pos = calcVisibleSide(target_elem, iframe, 5);

        //remove the transform first
        $(iframe).css({
            "-webkit-transform":"none",
            "-ms-transform":"none",
                "transform":"none"
        });

        if(!pos){
            //set to center
            $(iframe).css({
                "-webkit-transform":"translate(-50%,-50%)",
                "-ms-transform":"translate(-50%,-50%)",
                    "transform":"translate(-50%,-50%)",
                "left":"50%",
                "top":"50%"
            });
        }
        else if(smooth){
            //use animation
            $(iframe).animate({
                left: pos[0]+"px",
                top: pos[1]+"px"
            });
        }else{
            //set panel position
            $(iframe).css({
                left: pos[0],
                top: pos[1]
            });
        }
        
        //donot trigger open event here
    }
    
    /**UPDATE */
    //code here...
    
    /**INITIALIZE */
    function initTopNav(){
        const gdata = global_data;
        var iframe_win = gdata.iframeProperties.window,
            panel = gdata.iframeProperties.panel,
            top_nav = $(panel).find(" > .top-nav").first();
        var cpy_btn = $(top_nav).find("> .copy").first(),
            opt_btn = $(top_nav).find("> [data-name=option]").first(),
            inp = $(top_nav).find("> .output-text").first();
        //set to 1 
        var count = 0, opt_list = ["hwb", "hsl", "rgb", "hex", "cmyk"];
        
        //detect for unfocus
        $(inp).blur((_e)=>{
            /**
             * This to make sure that if the input is selected and once
             * the blur triggered it will unselect all the text
             */
            var cur_val = $(inp).val(),
            //you need to preserve its option
                cur_val = colorTo(cur_val, gdata.colorOption);

            $(inp)[0].setSelectionRange(0,0);
            //clear first
            $(inp).val("");
            //then set the current data
            $(inp).val(cur_val);
            
            //set to readonly
            $(inp).prop("readonly",true);
        });
        $(inp).on("input",()=>{
            var cur_val = $(inp).val();
            var cur_cursor_pos = $(inp)[0].selectionEnd;
            var color = window.w3color($(inp).val());
            
            //since the input set from set color
            setColor(color);
            
            //we need to reset the input from the current color value
            $(inp).val(cur_val);
            /**set the current cursor position next the value setter 
             * to preserve its position
            */
            $(inp)[0].selectionStart=cur_cursor_pos;
            $(inp)[0].selectionEnd=cur_cursor_pos;
        });
        $(inp).prop("readonly",true);
        
        $(inp).dblclick((e)=>{
            //exit if input is on editing mode
            if($(inp).is(":read-write")){return;}
            var color = gdata.color;
            //back to the previous color option
            count -= 2;
            
            //if count is less than -1 then the length of the list will be reduced based on 
            //the previous deduction to get back from the previous option
            count = count <= -1 ? opt_list.length + count : count;
            
            //set the new color option
            gdata.colorOption = opt_list[count];
            setOutputColor(color);

            //set write only
            $(inp).prop("readonly",false);
        });

        //for changing option
        $([$(inp)[0], $(opt_btn)[0]]).click(()=>{
            //you can't change the option once the input is on edit mode
            if(!$(inp).is("[readonly]")){return;}
            var color = gdata.color;
            //add new count
            count = count >= opt_list.length - 1 ? 0 : count + 1;
            
            //set the new color option
            gdata.colorOption = opt_list[count];
            setOutputColor(color);
        });

        //for copying the color
        $(cpy_btn).click(()=>{
            var color = gdata.color;
            var popup = $(cpy_btn).find("> .popup").first();
            
            copyToClipboard(colorTo(color, gdata.colorOption));
            //disable click
            $(cpy_btn).css("pointer-events","none");
            $(popup).stop();
            $(popup).fadeIn(100,()=>{
                //add delay
                $(popup)[0].custom = 0;
                $(popup).animate({custom:1},{
                    duration:1000,
                    complete:()=>{
                        $(popup).stop();
                        $(popup).fadeOut();
                        $(cpy_btn).css("pointer-events","all");
                    }
                })
            });
        });

        /**to trigger the input */
        $(iframe_win).keyup((e)=>{
            if(e.key.toLowerCase() == "enter" && $(inp).is(":read-write")){
                $(inp).blur();
            }
        });
    }
    
    //(Recycle Code)
    //Initialize all the adjuster
    function initAdjuster(){
        const gdata = global_data;
        //for iframe window
        var iframe_win = gdata.iframeProperties.window;
        var panel = gdata.iframeProperties.panel;
        //for vertical and horizontal element
            vr = $(panel).find("[data-type=adjuster][data-angle=vertical]"),
            hr = $(panel).find("[data-type=adjuster][data-angle=horizontal]");
        var onmousedown = null;
        
        /**(NOTE):
         * The calculation for thumb position is based on setAdjusterThumbPosition
         * for accuracy
         */

        /**init vertical first*/
        $(vr).on("mousedown touchstart", (e)=>{
            fixEventPosition(e);
            var cur_elem = e.currentTarget;
            var thumb = $(cur_elem).find("> .thumb").first();
            //get both adjuster and thumb width make sure to get thum outer size
            const a_height = $(cur_elem).height(), t_height = $(thumb).outerHeight();
            var crect = $(cur_elem)[0].getBoundingClientRect();
            
            onmousedown = (e)=>{
                var y = e.pageY - crect.top,
                //convert to percentage n <= 1 or n >= 0 (the real value)
                    pct = Math.max(Math.min(y / crect.height, 1), 0),
                //make sure to get adjuster's height rather than rectangle since this calculate the outer size
                    ypos = (a_height - t_height) * pct,
                    ypos = ypos / a_height;
                
                //set new position
                $(thumb).css("top",(ypos * 100)+"%");
                $(thumb).attr("title",((pct).toFixed(3) * 100)+"%");
                $(cur_elem).attr("data-value", pct);
                
                //add dataname to the trigger
                $(cur_elem).trigger("input", $(cur_elem).attr("data-name"));
            }       
            onmousedown(e);
        });
        
        /**init horizontal drag*/
        $(hr).on("mousedown touchstart", (e)=>{
            fixEventPosition(e);
            var cur_elem = e.currentTarget;
            var thumb = $(cur_elem).find("> .thumb").first();
            //get both adjuster and thumb width make sure to get thum outer size
            const a_width = $(cur_elem).width(), t_width = $(thumb).outerWidth();
            var crect = $(cur_elem)[0].getBoundingClientRect();
            
            onmousedown = (e)=>{
                var x = e.pageX - crect.left,
                //convert to percentage n <= 1 or n >= 0 (the real value)
                    pct = Math.max(Math.min(x / crect.width, 1), 0),
                    //calculate positon
                    xpos = (a_width - t_width) * pct,
                    //use the relative position
                    xpos = xpos / a_width;
                //set new position
                $(thumb).css("left",(xpos * 100)+"%");
                $(thumb).attr("title",((pct).toFixed(3) * 100)+"%");
                $(cur_elem).attr("data-value",pct);
                //add dataname to the trigger
                $(cur_elem).trigger("input", $(cur_elem).attr("data-name"));
            }          
            onmousedown(e);
        });
        
        $(iframe_win).on("mousemove touchmove",(e)=>{
            if(typeof(onmousedown) == "function"){
                fixEventPosition(e);
                onmousedown(e);
            }
        });
        
        //Best way to prevent page scrolling on drag (mobile)
        //https://github.com/bevacqua/dragula/issues/487#issuecomment-383857371
        iframe_win.document.addEventListener('touchmove', function(e) {
            //make sure to only check if onmousedown is function
            //to avoid outside from affecting from it
            if(typeof(onmousedown) == "function"){
                e.preventDefault(); 
            }
        }, { passive:false });
        
        //for cursor default and clear mousedown event
        $(iframe_win).on("mouseup touchend",(e)=>{
            var list = [...vr.toArray(), ...hr.toArray()];
            onmousedown=null;
        });
    }

    //initialize transparent, whiteness and blackness adjuster
    function initTCMYKdjuster(){
        const gdata = global_data;
        var panel = gdata.iframeProperties.panel;
        var center_nav = $(panel).find("> .container > .center-nav").first(),
            bottom_nav = $(panel).find("> .container > .bottom-nav").first();
        var trans_adjuster = $(center_nav).find(".transparent-adjuster").first(),
            cyan_adjuster = $(bottom_nav).find(".cyan-adjuster").first(), 
            magenta_adjuster = $(bottom_nav).find(".magenta-adjuster").first(),
            yellow_adjuster = $(bottom_nav).find(".yellow-adjuster").first(),
            key_adjuster = $(bottom_nav).find(".key-adjuster").first();

        //This set the opacity color using gdata color
        $(trans_adjuster).on("input",()=>{
            var val = parseFloat($(trans_adjuster).attr("data-value")),
                rev_val = 1 - val,
            //for w3_color
                w3_color = window.w3color(gdata.color),new_color;
            
            //set new opacity
            gdata.opacity = rev_val;
            //apply to w3 color
            w3_color.opacity = rev_val;
            
            //set the new output color
            setOutputColor(w3_color);
        });

        $(cyan_adjuster).on("input",(e)=>{
            const c = parseFloat($(e.currentTarget).attr("data-value")),
                m = gdata.magenta,
                y = gdata.yellow,
                k = gdata.key;
            const opacity = gdata.opacity;
            const cmyk_str = "cmyk("+c*100+"%,"+m*100+"%,"+y*100+"%, "+k*100+"%)";
            const w3_color = window.w3color(cmyk_str);
            
            //update cyan
            gdata.cyan = c;
            //store opacity
            w3_color.opacity = opacity;
            //update color here
            setOutputColor(w3_color);
            //update Transparent Background color
            setTAdjusterBackgroundColor(w3_color.toRgbString());
        });
        $(magenta_adjuster).on("input",(e)=>{
            const c = gdata.cyan,
                m = parseFloat($(e.currentTarget).attr("data-value")),
                y = gdata.yellow,
                k = gdata.key;
            const opacity = gdata.opacity;
            const cmyk_str = "cmyk("+c*100+"%,"+m*100+"%,"+y*100+"%, "+k*100+"%)";
            const w3_color = window.w3color(cmyk_str);
            
            //update cyan
            gdata.magenta = m;
            //store opacity
            w3_color.opacity = opacity;
            //update color here
            setOutputColor(w3_color);
            //update Transparent Background color
            setTAdjusterBackgroundColor(w3_color.toRgbString());
        });
        $(yellow_adjuster).on("input",(e)=>{
            const c = gdata.cyan,
                m = gdata.magenta,
                y = parseFloat($(e.currentTarget).attr("data-value")),
                k = gdata.key;
            const opacity = gdata.opacity;
            const cmyk_str = "cmyk("+c*100+"%,"+m*100+"%,"+y*100+"%, "+k*100+"%)";
            const w3_color = window.w3color(cmyk_str);
            
            //update cyan
            gdata.yellow = y;
            //store opacity
            w3_color.opacity = opacity;
            //update color here
            setOutputColor(w3_color);
            //update Transparent Background color
            setTAdjusterBackgroundColor(w3_color.toRgbString());
        });
        $(key_adjuster).on("input",(e)=>{
            const c = gdata.cyan,
                m = gdata.magenta,
                y = gdata.yellow,
                k = parseFloat($(e.currentTarget).attr("data-value"));
            const opacity = gdata.opacity;
            const cmyk_str = "cmyk("+c*100+"%,"+m*100+"%,"+y*100+"%, "+k*100+"%)";
            const w3_color = window.w3color(cmyk_str);
            
            //update cyan
            gdata.key = k;
            //store opacity
            w3_color.opacity = opacity;
            //update color here
            setOutputColor(w3_color);
            //update Transparent Background color
            setTAdjusterBackgroundColor(w3_color.toRgbString());
        });
    }

    //(Recycle Code)
    //this set element's focus that contain's data-autofocus
    function initAutoFocus(){
        const gdata = global_data;
        var panel = gdata.iframeProperties.panel;
        
        //for auto focus
        $(gdata.iframeProperties.window).on("mousedown touchdown",(e)=>{
            /**find all element that contains autofocus*/
            var list = $(panel).find("[data-autofocus]");
            $(list).each((_idx, elem) => {
                //check if contains
                if(elem.contains(e.target)){
                    /**set element to auto focus */
                    $(elem).attr("data-focus","true");

                    //trigger the event
                    $(elem).trigger("focus");
                }else{//otherwise remove data focus
                    if($(elem).is("[data-focus]")){

                        //trigger the event
                        $(elem).trigger("blur");
                    }
                    $(elem).removeAttr("data-focus");
                }
            });
        });

        /**this unfocus all if the window is not focus */
        $(gdata.iframeProperties.window).blur(()=>{
            var list = $(panel).find("[data-autofocus]");
            $(list).each((_idx, elem) => {
                //check if focus to trigger element blur event
                if($(elem).is("[data-focus]")){
                    //trigger the event
                    $(elem).trigger("blur");
                }
            });
            $(list).removeAttr("data-focus");
        });
    }

    //for shortcut
    function initShortcut(){
        const gdata = global_data;
        var iframe_win = gdata.iframeProperties.window;
        var cur_val = "";
    
        $(iframe_win).on("keydown",(e)=>{
            var elem ;
            //set to lower
            e.key = e.key.toLowerCase();
            
            //for removing character
            if(e.key=="backspace"){
                cur_val = cur_val.substring(0,cur_val.length - 1);
            }else if(e.key=="escape"){
                cur_val="";//reset
                return;
            }
            //check if number
            else if(!e.key.match(/^(\d|\.)$/)){return;}
            //check if already contains precision
            else if(e.key == "." && cur_val.match(/\./)){return;}
            else{
                //store
                cur_val += e.key;
            }
            
            if((elem = getFocusElement())){
                //check if hue adjuster
                if($(elem).is(".hue-adjuster")){
                    var val = Math.min(Math.max(parseFloat(cur_val), 0), 360),
                        val = isNaN(val) ? 0 : val;
                    var w3_color = window.w3color(gdata.color),
                        hsl = w3_color.toHsl(),
                        w3_color = window.w3color("hsl("+val+", "+(hsl.s * 100)+"%, "+(hsl.l * 100)+"%)");
                    //preserve its alpha
                    w3_color.opacity = gdata.opacity;
    
                    //set the new color
                    setColor(w3_color.toRgbaString());
                }else if($(elem).is(".transparent-adjuster")){//transparent adjuster
                    var val = Math.min(Math.max(parseFloat(cur_val), 0), 100),
                        val = isNaN(val) ? 0 : val / 100;
                    var w3_color = window.w3color(gdata.color);
                    //set new opacity
                    w3_color.opacity = val;
                    //set new color
                    setColor(w3_color.toRgbaString());
                }//for TWB adjuster
                else if($(elem).is(".cyan-adjuster") || 
                        $(elem).is(".magenta-adjuster") || 
                        $(elem).is(".yellow-adjuster") || 
                        $(elem).is(".key-adjuster") || 
                        $(elem).is(".transparent-adjuster")){
                    var val = Math.min(Math.max(parseFloat(cur_val), 0), 100),
                        val = isNaN(val) ? 0 : val / 100;
                    var w3_color = window.w3color(gdata.color);
                    //preserve its alpha
                    w3_color.opacity = gdata.opacity;
    
                    //set the whiteness value
                    if($(elem).is(".cyan-adjuster") || 
                        $(elem).is(".magenta-adjuster") || 
                        $(elem).is(".yellow-adjuster") || 
                        $(elem).is(".key-adjuster")){
                        //(NOTE): you only need to trigger the adjuster input to avoid from changing its current color
                        $(elem).attr("data-value",val);
                        //update trigger input to update output
                        $(elem).trigger("input");
                        //since by triggering input cannot change thumb position change it in here
                        setAdjusterThumbPosition(elem, val);
                        //Do not use this
                        //w3_color = window.w3color("hwb("+hwb.h+", "+(hwb.w * 100)+"%, "+(val * 100)+"%)");
                        //setColor(w3_color.toRgbaString());
                    }else{
                        //(NOTE): you only need to trigger the adjuster input to avoid from changing its current color
                        $(elem).attr("data-value",val);
                        //update trigger input to update output
                        $(elem).trigger("input");
                    }
                }
            }else{
                cur_val="";//reset the value
            }
        });
    
    
        //reset the value
        $(iframe_win).on("mousedown touchstart",()=>{
            cur_val="";
        });
    }

    //(Recycle Code)
    //initialize observer to register the element
    function initObserver(){
        const gdata = global_data;
        var iframe = gdata.iframeProperties.iframe;
        var panel = gdata.iframeProperties.panel;
        var top_nav = $(panel).find("> .top-nav").first()
        var top_nav_inp = $(top_nav).find("> .output-text").first();
        
        //For registering input
        function updateList(){
            $("input.mark-cmyk-color[type=button]").each((_, input)=>{
                //exit if already registered
                if($(input).is("[data-registered]") || $(input).data("_is_registered")){return;}
                
                /**REGISTER MUTATION OVBSERVER FOR CHANGE EVENT */
                //observer attributes
                // create a new instance of `MutationObserver` named `observer`,
                // passing it a callback function
                const observer = new MutationObserver(() => {
                    //check if already is visble
                    if($(iframe).is(":visible")){
                        //do nothing
                    }else{
                        //use rgb format
                        if(!gdata.transparentEnabled){
                            $(input).css("background-color",window.w3color($(input).attr("data-value")).toRgbString());
                        }else{//use rgba format
                            $(input).css("background-color",window.w3color($(input).attr("data-value")).toRgbaString());
                        }
                        $(input).trigger("change");
                    }
                });
                var click_callback, def_color = $(input).attr("data-value")||gdata.defaultColor,
                //if transparent not enabled
                    def_color = !gdata.transparentEnabled ? window.w3color(def_color).toRgbString() : 
                    //otherwise use rgba string
                        window.w3color(def_color).toRgbaString();
                    
                //add this before the observer added to avoid from triggering the change event
                $(input).attr("data-value", def_color);
                //set default background color
                $(input).css({
                    backgroundColor:def_color
                });
                
                // call `observe()` on that MutationObserver instance,
                // passing it the element to observe, and the options object
                observer.observe(input, {
                    attributeFilter: [ "data-value"],
                    subtree: false, childList: false
                });
                //for opening picker
                click_callback = ()=>{
                    var color = $(input).attr("data-value") || "blue";
                    gdata.targetElement = input;
                    showPicker(input);
                    //set new color
                    setColor(color);
                    //set btn new data value and its css
                    $(input).attr("data-value", gdata.color);
                    $(input).css("background-color", gdata.color);
                }
                //add public function
                input.showPicker = ()=>{
                    showPicker(input);
                };
                input.hidePicker = ()=>{
                    hidePicker();
                }
                //add click
                $(input).click(click_callback);

                /**
                 * Trigger the register event to notify that the input is registered
                 */
                $(input).trigger("register");
                //set to registered
                $(input).attr("data-registered","");
                //For security purpose
                $(input).data("_is_registered",true);
                //save observer
                $(input).data("_observer_ref", observer);
                $(input).data("_click_callback", click_callback);
            });
        }
        
        //update the registration list
        updateList();
        
        //Trigger target element's input
        $(top_nav_inp).on("change",()=>{
            const target_elem = gdata.targetElement;
            if(target_elem){
                var prev_color = $(target_elem).attr("data-value");
                var cur_color ;

                //for rgba
                if(gdata.transparentEnabled){
                    cur_color = gdata.w3color.toRgbaString();
                    
                    //to avoid duplication
                    if(prev_color == cur_color){
                        return;//exit
                    }
                }else{//rgb only
                    cur_color = gdata.w3color.toRgbString();
                    if(prev_color == cur_color){
                        return;//exit
                    }
                }
                
                //set btn new data value and its css
                $(target_elem).attr("data-value", cur_color);
                $(target_elem).css("background-color", cur_color);
                $(target_elem).trigger("input");
            }
        });
        
        //when panel close
        $(iframe).on("close",()=>{
            gdata.targetElement = null;
        });

        $(document, iframe).on("_updateelementlist",()=>{
            updateList();
        });
        //for unregistering element
        $(document, iframe).on("_unregisterelement",(_e, elem)=>{
            const gdata = global_data;

            if(!$(elem).is("input.mark-hwb-color[type=button]")){
                return null;
            }else if(!$(elem).data("_is_registered")){
                return null;
            }//check if target element
            else if($(gdata.targetElement)[0] == $(elem)[0]){
                gdata.targetElement = null;
                hidePicker();
            }

            try{
                //remove all handler and data from this element
                $(elem).data("_is_registered",null);
                $(elem).removeAttr("data-registered");
                $(elem).data("_observer_ref").disconnect();
                //ger callback ref
                $(elem).off("click",$(elem).data("_click_callback"));
            }catch(e){
                return false;
            }

            return true;
        });
    }
    
    //This initialize the auto hide panel panel's extension
    function initAutoHidePanel(){
        const gdata = global_data;
        const window_list = [];

        /**For hidding */
        //Note iframe's window cannot recieve event if the mouse position is
        //point to the main window.
        $(window).on("mousedown touchstart",(e)=>{
            //hide the picker
            hidePicker();
        });
        
        //store all the windows here including extension
        window_list.push(window);
        window_list.push(gdata.iframeProperties.window);
        //store extension window
        for(var key in gdata.extensions){
            if(typeof(gdata.extensions[key]._getProperties) == "function"){
                //get properties from attached extension
                const ext_prop = gdata.extensions[key]._getProperties(
                    //pass markCollection to access extension properties
                    window.markCollection
                );
                const ext_gdata = ext_prop.globalData;

                if(ext_gdata && ext_gdata.iframeProperties){
                    window_list.push(ext_gdata.iframeProperties.window);
                }
            }
        }

        //check all the window if not focused
        $(window_list).blur(()=>{
            var found_focus = false;
            const ext_list = gdata.extensions;
            //list all the attached iframe
            var list = [gdata.iframeProperties.document];
            
            //check all extension
            for(var key in ext_list){
                if(typeof(ext_list[key]._getProperties) == "function" &&
                    ext_list[key].attached){
                    //get properties from attached extension
                    const ext_prop = ext_list[key]._getProperties(
                        //pass markCollection to access color options properties
                        window.markCollection
                    );
                    const ext_gdata = ext_prop.globalData;
                    //check if extension includes iframe properties
                    if(ext_gdata.iframeProperties){
                        list.push(ext_gdata.iframeProperties.document);
                    }
                }
            }

            //check if one of them has focus to cancel from hidding the main panel
            for(var i in list){
                if(list[i].hasFocus()){
                    found_focus = true;
                    break;
                }
            }
            
            //cancel
            if(found_focus){
                return;
            }
            //hide once the window is not focused
            hidePicker();
        });
        
        //for extension iframes

        //check if window scrolled
        $(window).scroll(()=>{
            if(isVisible()){
                //remove the hide animation
                hidePicker(false);
            }
        });
    }
    
    //for extension's listener
    function initExtensions(){
        const gdata = global_data;
        const ext_list = gdata.extensions;

        //For color options extension
        (()=>{
            const iframe = gdata.iframeProperties.iframe;
            const panel = gdata.iframeProperties.panel;
            const btn = $(panel).find("> .top-nav [data-name=color-options]").first();
            //open extension
            $(btn).click((e)=>{
                if(ext_list._colorOptions && 
                    ext_list._colorOptions["attached"]){
                    const color_opt = ext_list._colorOptions;
                    //pass markCollection to access color options properties
                    const ext_prop = ext_list._colorOptions._getProperties(
                        window.markCollection
                    );
                    const cur_color = gdata.color;

                    //set color
                    color_opt.setColor(cur_color);
                    ext_prop.showPicker(iframe);
                }
            });

            //close extension once the picker closed
            $(iframe).on("close",()=>{
                //make sure to check if extension exist
                if(!ext_list._colorOptions){return;}

                //pass markCollection to access color options properties
                const ext_prop = ext_list._colorOptions._getProperties(
                    window.markCollection
                );
                ext_prop.hidePicker();
            });

            //check if ext_list._colorOptions is existed
            if(ext_list._colorOptions){
                const color_opt = ext_list._colorOptions;
                color_opt.on("input",(e)=>{
                    const data = e.data;
                    const color = data.value;
                    
                    //set new color
                    setColor(color);
                });
            }
        })();
    }
    
    /**(IMPORTANT NONE): for performance
     * Be aware of the onload blocking, wait till the user's main page loaded. 
     * It's not a big problem if the iframe content takes a short time 
     * to load (and execute), and the iframe in itself is good to use because it loads in parallel with 
     * the main page. But if it takes long for the iframe to finish, the user experience is damaged.
     * https://www.aaronpeters.nl/blog/iframe-loading-techniques-and-performance/
     * 
     * @param {*} callback once the iframe created
     */
    function initIframe(callback){
        const gdata = global_data;
        var iframe = document.createElement("iframe");
        //add html to the iframe
        const html = $("#mark-cmyk-color-picker").first().html();
        //return interval id. Make sure to add the DOM Element instead.
        const onElementResize = (elem, callback)=>{
            //store previous size
            var prev_size = [elem.offsetWidth, elem.offsetHeight];
            
            setInterval(()=>{
                //check the current size 
                var cur_size = [elem.offsetWidth, elem.offsetHeight ];

                for(var i in prev_size){
                    if(prev_size[i] != cur_size[i]){
                        //check if callback is function
                        if(typeof(callback) == "function"){
                            //pass current size
                            callback(cur_size);
                        }
                        prev_size[i] = cur_size[i];
                    }
                }
            },30);//set to 20 millisecond
        }, 
        name = "mark-cmyk-color-picker";
        //hide by default
        $(iframe).hide();
        //set attributes
        $(iframe).attr("id",name);
        $(iframe).attr("title","CMYK Color Picker");
        //allow transparency
        $(iframe).attr("allowtransparency","true");
        
        //set default css
        $(iframe).css({
            outline:"none",
            border:"none",
            position:"fixed"
        });
        
        //check if iframe is loaded
        $(iframe).on("load",()=>{
            //get the iframe document
            var iframe_docu = iframe.contentWindow.document;
            var body = $(iframe_docu).find("body").first();
            //for css link
            var css_link = document.createElement("link");
            css_link.setAttribute("rel", "stylesheet");
            css_link.setAttribute("type", "text/css");
            css_link.setAttribute("href", "./Css/Style.css");
            //set css to the main iframe
            iframe_docu.getElementsByTagName("head")[0].appendChild(css_link);

            /**When we make style in just one file, we need to identify which iframe to add the style to
             * so we add class name to the iframe's body
             */
            //add class name
            $(body).addClass(name);
            //add html here
            $(body).append(html);
            
            //find the panel
            $(iframe_docu).ready(()=>{
                var panel = $(body).find("> .panel");

                //Set window iframe, panel, and document.
                gdata.iframeProperties.iframe = iframe;
                gdata.iframeProperties.document = iframe_docu;
                gdata.iframeProperties.window = iframe.contentWindow;
                gdata.iframeProperties.panel = $(panel)[0];
                
                //check if panel is ready to get its size
                $(panel).ready(()=>{
                    
                    //update the iframe size, based on panel size
                    function updateIframeSize(){
                        $(iframe).css({
                            width: $(panel).outerWidth()+"px",
                            height: $(panel).outerHeight()+"px"
                        });
                    }

                    //we need to check wether the panel change its size to update iframe size
                    onElementResize($(panel)[0], (_sz)=>{
                        updateIframeSize();
                    });
                    updateIframeSize();
                    //Call the outside callback to initialize the JS
                    callback();
                });
            });
        });
        //add the iframe from HTML
        $("html").append(iframe);
    }
    
    //initialize all
    function init(){
        const gdata = global_data;
        initAutoHidePanel();

        //initialize observer to register the element
        initObserver();
        
        initTopNav();
        initAutoFocus();
        
        initAdjuster();
        //Initialize extensions
        initExtensions();

        //transparent, whiteness and blackness
        initTCMYKdjuster();

        //init shortcut next the adjuster
        initShortcut();
        
        //set the default color
        setColor(gdata.defaultColor);
        
        //trigger after all the function is initialize
        if(typeof(gdata.onready) == "function"){
            gdata.onready();
        }
    }
    
    //(Recycle Code)
    /**
     * This initialize the properties of this picker from the collection immediatly
     */
    (function initProperties(){
        const gdata = global_data;
        var mark_collection = window.markCollection;
        const callback_list = [];
        const ext_list = ["_colorOptions"];
        
        //if already registered then use the existing otherwise create new one
        mark_collection = mark_collection && typeof(mark_collection) == "object" ? window.markCollection : {};
        
        //check if not yet added the updateList
        if(typeof(mark_collection.updateList) != "function"){
            //update to this script itself
            mark_collection.updateList = ()=>{
                $(document).trigger("_updateelementlist");
            }
        }
        
        //check if not yet added the unregisterInput
        if(typeof(mark_collection.unregisterInput) != "function"){
            mark_collection.unregisterInput = (elem)=>{
                $(document).trigger("_unregisterelement",elem);
            }
        }

        /**for callback's method list*/
        function addCallback(name, callback_ref){
            callback_list.push({
                eventName: name,
                callback: callback_ref
            });
        }

        /**
         * @param {*} name event name
         * @param {*} callback_ref if have callback
         * @return array
         */
        function getCallbacks(name,callback_ref){
            const arr = [];

            //find from the list if ref contains callback
            if(typeof(callback_ref) == "function"){
                for(var i in callback_list){
                    var val = callback_list[i];
                    if(val.eventName == name && val.callback == callback_ref){
                        //return the callback
                        arr.push(val.callback);
                    }
                }
            }else{//if no reference
                for(var i in callback_list){
                    var val = callback_list[i];
                    if(val.eventName == name){
                        //return the callback
                        arr.push(val.callback);
                    }
                }
            }
            return arr;
        }
        
        /**
         * If callback is not defined then 
         *  all callback attached to event name will be remove
         * @param {*} name event name
         * @param {*} callback_ref if have callback
         */
        function removeCallback(name,callback_ref){

            //find from the list if ref contains callback
            if(typeof(callback_ref) == "function"){
                for(var i = 0; i < callback_list.length; i++){
                    var val = callback_list[i];

                    if(val.eventName == name && val.callback == callback_ref){
                        //remove the callback
                        callback_list.splice(i,1);
                        //step back
                        i -= 1;
                    }
                }
            }else{//if no reference
                for(var i = 0; i < callback_list.length; i++){
                    var val = callback_list[i];
                    if(val.eventName == name){
                        //remove the callback
                        callback_list.splice(i,1);
                        //step back
                        i -= 1;
                    }
                }
            }
        }
        //----
        
        /**for the on ready event*/
        //this event dispatch once the main element is ready or added to DOM
        gdata.onready = ()=>{
            //Set all default properties here...
            //set is ready to true
            mark_collection.cmykColorPicker.isReady = true;
            //hide ColorOptions, this because it is disabled by default
            mark_collection.cmykColorPicker.enableColorOptions(false);
            //--end of default properties--
            
            //trigger the event once the cmykColorPicker is ready
            $(document).trigger("_cmykcolorpickerisready");
        }
        
        //store CMYKColorPicker Properties
        mark_collection.cmykColorPicker = {
            /**This update all unregistered element*/
            updateList(){
                var iframe = gdata.iframeProperties.iframe;
                // dispatch the event here
                if(iframe && $(iframe)[0]){
                    $(iframe).trigger("_updateelementlist");
                }
            },
            /**
             * Once the HSL color picker is ready
             * @param {*} callback 
             * @returns undefined
             */
            ready(callback){
                //exit if not function
                if(typeof(callback) != "function"){return;}
                //trigger once the cmykColorPicker is ready
                else if(this.isReady){
                    //callback the reference
                    callback();
                }else{
                    //Trigger only once, once the cmykColorPicker is ready or added
                    //The event handler function is only run ONCE for each element.
                    $(document).one("_cmykcolorpickerisready",()=>{
                        //callback the user reference
                        callback();
                    });

                    //store callback
                    addCallback("ready", callback);
                }
            },
            showPicker(){
                const target_elem = gdata.targetElement;
                showPicker(target_elem);
            },
            hidePicker(){
                hidePicker();
            },
            //return if the picker is visible
            isVisible(){return isVisible();},
            getColor(){
                return global_data.color;
            },
            //this performancec is slow
            setColor(color){
                setColor(color);
            },
            //return registered input list
            getList(){
                return $("input.mark-hsl-color[type=button][data-registered]").toArray();
            },
            //get the value from target input
            getValue(){
                return $(gdata.targetElement).attr("data-value");
            },
            getW3Color(){
                return global_data.w3color;
            },
            //get data from picker (faster)
            getData(){
                const gdata = global_data;
                return {
                    color: gdata.color, 
                    cyan: gdata.cyan, 
                    magenta: gdata.magenta, 
                    yellow: gdata.yellow, 
                    key: gdata.key, 
                    opacity: gdata.opacity
                };
            },
            //return the panel's position
            getPickerPosition(){
                var iframe = gdata.iframeProperties.iframe;
                if(iframe){
                    var rect = iframe.getBoundingClientRect();
                    return {x: rect.left, top: rect.top};
                }
                return null;
            },
            //set the panel's position
            setPickerPosition(x,y){
                var iframe = gdata.iframeProperties.iframe;
                if(iframe){
                    //remove the transform
                    $(iframe).css({
                        "-webkit-transform":"none",
                        "-ms-transform":"none",
                            "transform":"none"
                    });

                    $(iframe).css({
                        left: x+"px",
                        top: y+"px"
                    });
                }
            },
            setPickerPositionTo(elem, smooth=false){
                setPickerPositionTo(elem, smooth)
            },
            /**Other properties */
            enableCopy(val=true){
                var panel = gdata.iframeProperties.panel;
                var cpy_btn = $(panel).find("> .top-nav > .copy").first(),
                    prev_style;
                //check if existed
                if(!$(cpy_btn)[0]){return;}
                /**
                 * Apparently you can't set important to css, that's why need
                 * to get the style and add to the current style
                 * https://stackoverflow.com/questions/1986182/how-to-include-important-in-jquery
                 */
                //store the previous style first
                prev_style = $(cpy_btn).attr("style"),
                prev_style = !prev_style ? "" : $(cpy_btn).attr("style");
                $(cpy_btn).css("cssText", prev_style + " display: "+(val ? "block" : "none")+" !important;");
            },
            //if the value is not defined then the default will be setted
            setTheme(val="light"){
                var iframe_docu = gdata.iframeProperties.document;
                //find the body from iframe document, to apply theme
                //This is due to the fact that the panel was added in the body from the iframe
                $(iframe_docu).find("body").attr("data-theme",val);
            },
            enableTransparent(val=true){
                var panel = gdata.iframeProperties.panel;
                var trans_adjuster = $(panel).find("> .container > .center-nav > .transparent-adjuster").first(),
                    prev_style = $(trans_adjuster).attr("style");

                //check if existed
                if(!$(trans_adjuster)[0]){return;}
                //set transparent status
                gdata.transparentEnabled = val;
                /**
                 * Apparently you can't set important to css, that's why need
                 * to to get the style and add to the current style
                 * https://stackoverflow.com/questions/1986182/how-to-include-important-in-jquery
                 */
                //store the previous style first
                prev_style = $(trans_adjuster).attr("style"),
                prev_style = !prev_style ? "" : $(trans_adjuster).attr("style");
                $(trans_adjuster).css("cssText", prev_style + " display: "+(val ? "block" : "none")+" !important;");
            },
            //EXTENSION:
            //this enable more option
            //the default is disabled
            enableColorOptions(val=true){
                var ext_list = gdata.extensions;
                const panel = gdata.iframeProperties.panel;
                const btn = $(panel).find("> .top-nav [data-name=color-options]").first();
                var option = typeof val == "object" ? val||option||{} : {};

                //hide this first
                $(btn).hide();
                
                //check if defined
                if(typeof(ext_list._colorOptions) == "object"){
                    //check override configuration
                    for(var key in option){
                        switch(key){
                            case "theme":
                                ext_list._colorOptions.setTheme(option[key]);
                                break;
                            case "enableColorShades":
                                ext_list._colorOptions.enableColorShades(option[key]);
                                break;
                            case "enableRecentColors":
                                ext_list._colorOptions.enableRecentColors(option[key]);
                                break;
                            case "enableBasicColors":
                                ext_list._colorOptions.enableBasicColors(option[key]);
                                break;
                            case "basicColors":
                                //check if array first
                                if(Array.isArray(option[key]) && option[key].length){
                                    ext_list._colorOptions.setBasicColors(option[key]);
                                }
                                break;
                            case "recentColors":
                                //check if array first
                                if(Array.isArray(option[key]) && option[key].length){
                                    ext_list._colorOptions.setRecentColors(option[key]);
                                }
                                break;
                        }
                    }
                    //--end--

                    //set to attached
                    if(val){
                        $(btn).show();
                        ext_list._colorOptions["attached"] = true;
                    }else{
                        $(btn).hide();
                        ext_list._colorOptions["attached"] = false;
                    }
                }else{
                    throw "Color options not defined";
                }
            },
            unregisterInput(elem){
                $(global_data.iframeProperties.iframe).trigger("_unregisterelement",elem);
            },
            isVisible(){
                return global_data.isVisible;
            },
            //style can be link or src/path
            //if include_ext is true then this include to extensions available as well
            addStyle(style, include_ext=false){
                //means not yet initialized or ready
                if(!(gdata.iframeProperties && gdata.iframeProperties.document)){return;}
                var iframe_docu = gdata.iframeProperties.document;
                
                //check if contains href
                if(typeof(style) == "object" && $(style).is("link[href]")){
                    iframe_docu.getElementsByTagName("head")[0].appendChild(style);
                }else if(typeof(style) == "string"){
                    var css_link = document.createElement("link");
                    css_link.setAttribute("rel", "stylesheet");
                    css_link.setAttribute("type", "text/css");
                    css_link.setAttribute("href", style);
                    
                    //set css to the main iframe
                    iframe_docu.getElementsByTagName("head")[0].appendChild(css_link);
                }
            },
            //For event
            on(event_name, callback){
                var iframe = gdata.iframeProperties.iframe;
                var panel = gdata.iframeProperties.panel;
                if(typeof(callback) != "function"){return;}

                switch(event_name){
                    case "ready":
                        //exit if not function
                        if(typeof(callback) != "function"){return;}
                        //trigger once the colorPicker is ready
                        else if(this.isReady){
                            //callback the reference
                            callback(this);
                        }else{
                            //Trigger only once, once the colorPicker is ready or added
                            //The event handler function is only run ONCE for each element.
                            $(document).one("_cmykcolorpickerisready",()=>{
                                //callback the user reference
                                callback(this);
                            });
                        }
                        break;
                    case "input":
                        var top_nav = $(panel).find("> .top-nav").first();
                        var inp = $(top_nav).find("> .output-text").first();
                        
                        $(inp).on("change",{
                            eventName: "input",
                            //make sure to enclose with get for realtime update
                            get value(){
                                return gdata.color;
                            },
                            get w3color(){
                                return gdata.w3color;
                            },
                            handler: callback
                        },callback);
                        break;
                    case "draw":
                        $(iframe).on("open",{
                            eventName: "draw",
                            handler: callback
                        },callback);
                        break;
                    case "hide":
                        $(iframe).on("close",{
                            eventName: "hide",
                            handler: callback
                        },callback);
                        break;
                    case "editing":
                        var top_nav = $(panel).find("> .top-nav").first();
                        var inp = $(top_nav).find("> .output-text").first();
                        //Pass data to the handler. User can retrieve by event.data when an event is triggered.
                        $(panel).find("[data-type=adjuster]").each((_, elem)=>{
                            $(elem).on(
                                "input",{
                                    eventName: "editing",
                                    name : $(elem).attr("data-name"),
                                    //set the value
                                    get value (){
                                        switch($(elem).attr("data-name")){
                                            case "hue": return gdata.hue;
                                            case "whiteness": return gdata.whiteness;
                                            case "blackness": return gdata.blackness;
                                            case "transparent": return gdata.opacity;
                                        }
                                    },
                                    handler: callback
                                },callback //add user callback
                            );
                        });
                        //Pass data to the handler. User can retrieve by event.data when an event is triggered.
                        $(inp).on("input",{
                            eventName: "editing",
                            name : "color",
                            //set the value
                            get value (){
                                return gdata.color;
                            },
                            handler: callback
                        },callback);
                        break;
                    
                }

                //add the callback
                addCallback(event_name, callback);
            },
            //off event
            off(event_name, callback){
                var iframe = gdata.iframeProperties.iframe;
                var panel = gdata.iframeProperties.panel;
                //This return array of callback if no reference or found same callback
                var callbacks = getCallbacks(event_name, callback);

                switch(event_name){
                    case "input":
                        var top_nav = $(panel).find("> .top-nav").first();
                        var inp = $(top_nav).find("> .output-text").first();
                        for(var i in callbacks){
                            $(inp).off("change",callbacks[i]);
                        }
                        break;
                    case "draw":
                        for(var i in callbacks){
                            $(iframe).off("open",callbacks[i]);
                        }
                        
                        break;
                    case "hide":
                        for(var i in callbacks){
                            $(iframe).off("close",callbacks[i]);
                        }
                        
                        break;
                    case "editing":
                        var top_nav = $(panel).find("> .top-nav").first();
                        var inp = $(top_nav).find("> .output-text").first();
                        
                        for(var i in callbacks){
                            //find all the adjuster
                            $(panel).find("[data-type=adjuster]").off("input",callbacks[i]);
                            $(inp).off("input",callbacks[i]);
                        }
                        
                        break;
                    
                }

                //Note that if the callback is not defined then 
                //all callback based on event name will be remove
                removeCallback(event_name, callback);
            },
            isReady: false,
            get version(){return "version 1.0"},
        };

        //store all the extensions to global data
        for(var i in ext_list){
            var key = ext_list[i];
            //ignore if not included
            if(!mark_collection[ext_list[i]]){continue;}

            //clone it
            gdata.extensions[key] = {...mark_collection[ext_list[i]]};

            //make sure to set attached to false
            gdata.extensions[key]["attached"] = false;
        }

        /**test extension*/
        // mark_collection.cmykColorPicker.ready(()=>{
        //     mark_collection.cmykColorPicker.enableColorOptions({
        //         enableColorShades: false,
        //         basicColors:["yellow","pink","white"],
        //         recentColors:["red","yellow"],
        //     });
        // });
        mark_collection.cmykColorPicker.on("ready",()=>{
            mark_collection.cmykColorPicker.setTheme("coral");
            mark_collection.cmykColorPicker.addStyle("./Css/Themes.css");
        });

        //set third party window.w3color
        mark_collection.w3color = typeof(window.w3color) == "function" ? window.w3color : undefined;
        //store or update the new collection
        window.markCollection = mark_collection;
    })();
    
    $(document).ready(()=>{
        
        initIframe(()=>{
            init();
        });
    });
})();